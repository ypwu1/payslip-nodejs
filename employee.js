

class Employee {
    constructor( firstName, lastName,  annualSalary,  superRate,  paymentStartDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.annualSalary = annualSalary;
        this.superRate = fromPercentageStringToDouble(superRate);
        this.paymentStartDate = paymentStartDate;
        //
        this.netIncome = 0;
        this.incomeTax = 0;
        this.superAmount = 0;
        this.grossIncome = 0;
    }

}

let fromPercentageStringToDouble = str => {
    try {
        // console.log(str);
        if(!str.endsWith("%")){
            throw new Error('Could not parse Super rate!');
        }else {
            return Number(str.substring(0,str.length-1))/100 ;
        }
    }catch(e){
        throw e;
    }

};

let toEmployeeList = data => {
    try {
        let rows = data.toString().split('\n');
        let employees = [];
        for(const row of rows){
            let fields = row.split(',');
            employees.push(new Employee(fields[0],fields[1],fields[2],fields[3],fields[4]));
        }
        return Promise.resolve(employees);
        
    }catch(e){
        return Promise.reject(e);
    }
};

module.exports = {
    Employee,
    toEmployeeList,
    fromPercentageStringToDouble
};