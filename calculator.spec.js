let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;

let divisions = require('./calculator').divisions;
let getDivision = require('./calculator').getDivision;
let incomeTax = require('./calculator').incomeTax;
let grossIncome = require('./calculator').grossIncome;
let netIncome = require('./calculator').netIncome;
let getSuper = require('./calculator').getSuper;
let format = require('./calculator').format;
let Employee = require('./employee').Employee;

let compose = require('./util').compose;

describe('calulator', () => {
    let employee;

    before(() => {
        employee = new Employee("Oscar", "Wu", '60050', '40%', "01 March - 31 March");
    });

    it('returns division of employee', () => {
         let division = getDivision(employee);
        expect(division).to.equal(divisions()[1]);

    });

    it('returns taxable income of employee', () => {

        expect(incomeTax(employee).incomeTax).to.equal(922);

    });

    it('returns gross income of employee', () => {
        expect(grossIncome(employee).grossIncome).to.equal(5004);


    });

    it('returns net income of employee', () => {
        employee = grossIncome(incomeTax(employee));
        expect(netIncome(employee).netIncome).to.equal(4082);


    });

    it('returns super of employee', () => {
        expect(compose(getSuper,grossIncome)(employee).superAmount).to.equal(2002)

    });
    
});
