let TaxDivision = require('./TaxDivision');

const DIVISIONS = [
    new TaxDivision(18200, 37000, 0.19, 0),
    new TaxDivision(37000, 80000, 0.325, 3572),
    new TaxDivision(80000, 180000, 0.37, 17547),
    new TaxDivision(180000, Number.MAX_SAFE_INTEGER, 0.45, 54547)
];

let divisions = () => {
    return DIVISIONS;
};

let getDivision = employee => divisions()
    .find(
        taxDivision => employee.annualSalary > taxDivision.minLimit && employee.annualSalary <= taxDivision.maxLimit
    );


let incomeTax =  employee => {
    let division = getDivision(employee);
    if(employee.annualSalary < 0) {
        throw new Error('Invalid annual salary range!');
    }
    employee.incomeTax  = Math.round((division.additionCost + (employee.annualSalary - division.minLimit) * division.taxRate) / 12);
    return employee;
};

let grossIncome = employee => {
    employee.grossIncome = Math.round(employee.annualSalary/12);
    return employee;
};

let netIncome = employee => {
    employee.netIncome = employee.grossIncome - employee.incomeTax;
    return employee;
};

let getSuper = employee => {
    employee.superAmount = Math.round(employee.grossIncome * employee.superRate);
    return employee;
};

let format = employee => {
    return employee.firstName + ' ' + employee.lastName + ',' + employee.paymentStartDate + ',' + employee.grossIncome + ',' +
        employee.incomeTax + ',' + employee.netIncome + ',' + employee.superAmount;
};

module.exports = {
    divisions,
    getDivision,
    incomeTax,
    grossIncome,
    netIncome,
    getSuper,
    format
};