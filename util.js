let fs = require('fs');

let readFrom = path => {

    let p = new Promise((resolve, reject) => {
        try {
            let readStream = fs.createReadStream(path);
            let data;
            readStream.on('data', (_data) => {
                data = Buffer.from(_data,'utf8');
            });
            readStream.on('close', () => {
                resolve(data);
            });
        } catch (e) {
            console.error(e);
            reject(e);
        }
    });

    return p;
};

let writeTo = (path) => (content) => {
    try {
        let writeStream = fs.createWriteStream(path, {flags: 'w'});
        writeStream.on('finish', () => {
            console.log('finish');
        });
        writeStream.write(content);
        writeStream.end();
    } catch (e) {
        console.error(e);
        throw e;
    }
};


let getInputAndOutput = () => {
    try {
        if (['--i', '--input'].indexOf(process.argv[2]) == -1) {
            throw(new Error());
        }
        if (['--o', '--output'].indexOf(process.argv[4]) == -1) {
            throw(new Error());
        }
        return [process.argv[3], process.argv[5]];
    } catch (e) {
        console.error('usage: node app.js --i input.csv --o output.csv');
        process.exit(1);
    }
};

let compose = (f, g) => {
    return arg => {
        return f(g(arg));
    };
};

module.exports = {
    readFrom,
    getInputAndOutput,
    writeTo,
    compose
};