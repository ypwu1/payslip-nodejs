class TaxDivision {
    constructor(minLimit, maxLimit, taxRate, additionCost) {
        this.minLimit = minLimit;
        this.maxLimit = maxLimit;
        this.taxRate = taxRate;
        this.additionCost = additionCost;
    }
}

module.exports = TaxDivision;