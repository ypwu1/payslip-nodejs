let readFrom = require('./util').readFrom;
let writeTo = require('./util').writeTo;
let getInputAndOutput = require('./util').getInputAndOutput;
let toEmployeeList = require('./employee').toEmployeeList;
let compose = require('./util').compose;
let format  = require('./calculator').format;
let getSuper  = require('./calculator').getSuper;
let netIncome  = require('./calculator').netIncome;
let grossIncome  = require('./calculator').grossIncome;
let incomeTax  = require('./calculator').incomeTax;

let app = () => {
    let [i,o] = getInputAndOutput();
  readFrom(i)
      .then(toEmployeeList)
      .then(employees =>{
           let payslips = employees.map(employee => compose(format,compose(getSuper,compose(netIncome, compose(grossIncome,incomeTax))))(employee)).join('\n');
          return Promise.resolve(payslips);
      })
      .then(writeTo(o));
};

app();
