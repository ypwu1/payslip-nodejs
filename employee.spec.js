
let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;

let Employee = require('./employee').Employee;
let toEmployeeist = require('./employee').toEmployeeList;

describe('employee', () => {
    it('creates new employee', () => {
        let e = new Employee("Oscar", "Wu", '65000', '0.4%', "01 March - 31 March");
        expect(e.firstName).to.equal('Oscar');
    });

    it('generates new employee list from input stream', () => {
        let input = 'David,Rudd,60050,9%,01 March – 31 March\nRyan,Chen,120000,10%,01 March – 31 March';
        expect(toEmployeeist(input).then(employees => employees.length)).to.eventually.equal(2);
    });



});

