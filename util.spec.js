let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;

let readFrom = require('./util').readFrom;
let compose= require('./util').compose;
let getInputAndOutput= require('./util').getInputAndOutput;

describe('Test readCsv', () => {

    it('reads a csv file', () => {

        return expect(readFrom('foo.txt').then(buffer => buffer.toString())).to.eventually.equal('foo');

    });

    it('helps f compose g', () => {
        let f = x => x + 1;
        let g = x => x * 2;
        expect(compose(f,g)(1)).to.equal(3);
        expect(compose(g,f)(1)).to.equal(4);



    });


    it('reads input/output paths', () => {
        let mockCommand = 'node app.js --i input.csv --o output.csv';
        process.argv = mockCommand.split(' ');
        let [i,o] = getInputAndOutput();
        expect(i).to.equal('input.csv');
        expect(o).to.equal('output.csv');
    })


});
